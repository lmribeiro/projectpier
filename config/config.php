<?php
  define('DB_ADAPTER', 'mysql'); 
  define('DB_HOST', 'localhost'); 
  define('DB_USER', 'root'); 
  define('DB_PASS', 'root'); 
  define('DB_NAME', 'pp'); 
  define('DB_PREFIX', 'pp_'); 
  define('DB_CHARSET', 'utf8'); 
  define('DB_PERSIST', false); 
  return true;
?>